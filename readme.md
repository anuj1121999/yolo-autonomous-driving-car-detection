This project implements the YOLO(You Only Look Once) Algorithm with the help of a pre-trained 
convolutional neural network.The YOLO algorithm is a Real Time Object Detection Algorithm which is used
for detection of objects for an Autonomous driving Car in this project.The project was done as a part of course deeplearning.ai
The dataset is provided by drive.ai.
It is implemented in python along with TensorFlow and Keras.